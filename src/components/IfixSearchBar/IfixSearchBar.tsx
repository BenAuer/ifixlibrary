import React, { Dispatch, SetStateAction } from "react";
import { useCallback } from "react";
import "./IfixSearhBar.css";
import debounce from "lodash.debounce";

interface SearchBarProps {
  setItems: Dispatch<SetStateAction<any[] | null>>;
}

export const IfixSearchBar: React.FC<SearchBarProps> = props => {
  const { setItems } = props;

  // Search Function
  const handleSearch = useCallback(
    debounce((query: string) => {
      if (!query) {
        setItems(null);
        return;
      }
      fetch(
        `https://www.ifixit.com/api/2.0/suggest/${encodeURI(
          query
        )}?doctypes=guide`
      )
        .then(res => res.json())
        .then(res => setItems(res.results));
    }, 250),
    [setItems]
  );

  // Get Input Function
  const handleSearchInputChange = (e: React.FormEvent<HTMLInputElement>) => {
    handleSearch(e.currentTarget.value);
  };

  // Return/Render Statement
  return (
    <div className="search-bar-wrapper">
      <input
        type="text"
        className="search-input"
        onChange={handleSearchInputChange}
      />
    </div>
  );
};
