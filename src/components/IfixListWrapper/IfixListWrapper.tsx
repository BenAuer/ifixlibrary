import React, { ReactNode, useState } from "react";
import { IfixHeader } from "../IfixHeader/IfixHeader";
import { IfixList } from "../IfixList/IfixList";
import { IfixSearchBar } from "../IfixSearchBar/IfixSearchBar";
import { IfixFooter } from "../IfixFooter/IfixFooter";

interface IfixListWrapperProps {
  children?: ReactNode;
}

export const IfixListWrapper: React.FC<IfixListWrapperProps> = () => {
  const [searchResults, setSearchResults] = useState<any[] | null>(null);

  // Return/Render Statement
  return (
    <div>
      <IfixHeader />
      <IfixSearchBar setItems={setSearchResults} />
      <IfixList searchResults={searchResults} />

      <IfixFooter />
    </div>
  );
};
