import React from 'react';
import {ReactNode, useState, useEffect} from 'react';
import {  GuideOverView, Comments, GuideDetails } from '../../types/types';
import './IfixListItemDetails.css';


interface IfixListItemDetailsProps {
		item: GuideOverView;
		children?: ReactNode;
}


export const IfixListItemDetails: React.FC <IfixListItemDetailsProps> = (props) => {
		const {item} = props;
		const [details, setDetails] = useState<Comments & GuideDetails | undefined>(undefined);

		useEffect(() => {
				fetch('https://www.ifixit.com/api/2.0/guides/' + item.guideid)
					.then(response => {
						if (response.ok) {
							return response.json();
						}
						throw new Error("Request failed!");
					})
					.then((details: Comments & GuideDetails ) => setDetails(details));
			}, [item.guideid]);

		if (!details) {
			return (
				<div>Loading... Takes a while... you know?</div>
			);
		}
			console.log(details);

		// Return/Render Statement
		return (
			<div className="details-wrapper">
				<div className="general-info">
					<span>{details.author.unique_username}</span>
					<span>{details.category}</span>
					<span>{details.difficulty}</span>
				</div>

				<div className="tool-list-wrapper">
					You'll need the following tools: 
					<ul className="tool-list-parent">
						{
							details.tools.map((tool, index) => 
							<li key={index + 10}>{ tool.quantity + ' ' + tool.text}</li>
							)
						}
						
					</ul>
				</div>

				

				<div className="introduction-parent">
					<p>{details.introduction_raw}</p>
					<img 
						src={details.image.medium} 
						alt="the hardware"
					/>
				</div>

				<ul>
					{
						details.steps.map((step, stepIndex) => 
							<li 
								className="step"
								key={`Step-${step.stepid}`}
							>
								<div className="step-headline">
									Step {stepIndex + 1} 
								</div>
								{ 
										step.lines.map((line, index) => 
										<p key={`Step-${index}`}>{line.text_raw}</p>
							
										)
								}
							</li>
						) 
					}				
				</ul>

				<div className="conclusion-parent">
					{details.conclusion_raw}
				</div>
				
			</div>
		);
}