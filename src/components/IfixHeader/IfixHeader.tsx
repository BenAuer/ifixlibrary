import React from "react";
import "./IfixHeader.css";

export const IfixHeader: React.FC = () => {
  // Return/Render Statement
  return (
    <div className="header-title-parent">
      <h1 className="header-title">iFixit Library</h1>
    </div>
  );
};
