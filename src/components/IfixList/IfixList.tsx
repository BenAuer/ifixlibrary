import React, { useState } from "react";
import { IfixListItem } from "../IfixListItem/IfixListItem";
import { GuideOverView } from "../../types/types";
import InfiniteScroll from "react-infinite-scroller";

interface IfixListProps {
  searchResults: any[] | null;
}

const PAGE_SIZE = 20;

export const IfixList: React.FC<IfixListProps> = props => {
  const { searchResults } = props;

  // Items State - API Content
  const [items, setItems] = useState<GuideOverView[]>([]);
  // State Scroll
  const [isFetching, toggleIsFetching] = useState(false);
  // Check if API has more
  const [hasMore, toggleHasMore] = useState(true);

  const fetchMoreItems = async (pageNumber: number) => {
    toggleIsFetching(true);
    try {
      const newItems = await fetch(
        `https://www.ifixit.com/api/2.0/guides?offset=${pageNumber *
          PAGE_SIZE}&limit=${PAGE_SIZE}`
      ).then(response => {
        if (response.ok) {
          return response.json();
        }
        throw new Error("Request failed!");
      });
      setItems(prevState => [...prevState, ...newItems]);
      if (newItems.length === 0) {
        toggleHasMore(false);
      }
    } catch (error) {
      console.error(error);
    } finally {
      toggleIsFetching(false);
    }
  };

  // Return/Render Statement
  return (
    <div className="ifix-list">
      {searchResults ? (
        searchResults.map(res => <IfixListItem key={res.guideid} item={res} />)
      ) : (
        <InfiniteScroll
          pageStart={0}
          loadMore={fetchMoreItems}
          hasMore={hasMore}
          loader={
            isFetching ? (
              <div className="loader" key="loader">
                Page is loading...
              </div>
            ) : (
              (null as any)
            )
          }
        >
          {items.map(item => (
            <IfixListItem key={item.guideid} item={item} />
          ))}
        </InfiniteScroll>
      )}
    </div>
  );
};
