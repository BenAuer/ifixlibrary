import React from "react";
import "./IfixFooter.css";

export const IfixFooter: React.FC = () => {
  // Return/Render Statement
  return (
    <div className="footer-wrapper">
      <div className="footer-parent">Footer for Imprint and Stuff....</div>
    </div>
  );
};
