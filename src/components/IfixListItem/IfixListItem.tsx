import React from "react";
import { ReactNode, useState } from "react";
import { GuideOverView } from "../../types/types";
import "./IfixListItem.css";
import { IfixListItemDetails } from "../IfixListItemDetails/IfixListItemDetails";

interface IfixListItem {
  item: GuideOverView;
  children?: ReactNode;
  key: any;
}

export const IfixListItem: React.FC<IfixListItem> = props => {
  const { item } = props;
  const [detailsVisible, setDetailsVisible] = useState(false);

  // Toggle Function
  const toggleDetails = () => {
    setDetailsVisible(oldState => !oldState);
  };

  // Return/Render Statement
  return (
    <div className="list-item-parent" onClick={toggleDetails}>
      <li className="list-item">{item.title}</li>

      {detailsVisible ? <IfixListItemDetails item={item} /> : null}
    </div>
  );
};
