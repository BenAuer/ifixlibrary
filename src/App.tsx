import React from 'react';
import './App.css';
import {IfixListWrapper} from './components/IfixListWrapper/IfixListWrapper';


const App: React.FC = () => {
  return (
    <div className="App">
      <IfixListWrapper />
    </div>
  );
}

export default App;
