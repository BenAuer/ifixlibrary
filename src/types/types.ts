interface Title {
    title: string;
}

interface Image {
    id: string;
    guid: string;
    mini: string;
    thumbnail: string;
    '140x105': string;
    '200x150': string;
    standard: string;
    '440x330': string;
    medium: string;
    large: string;
    huge: string;
    original: string;
}

interface Step {
    title: string;
    lines: Line[];
    stepid: number;
}

interface Line {
    text_raw: string;
    bullet: string;
    level: number;
    lineid: string | null;
    text_rendered: string;
}

interface Media {
    type: string,
    data: Image[] | Comments[];
}

export interface Comments {
    commentid: number;
    locale: string;
    context: string;
    contextid: number;
    partnerid: number | null;
    author: Author;
    title: string;
    text_raw: string;
    rating: number;
    date: Date | number;
    modified_date: Date | number;
    replied_date: Date | number;
    status: string;
    replies: [];
}

interface Author {
    userid: number;
    username: string;
    unique_username: string | null;
    join_date: Date | null;
    image: Image;
    reputation: number;
    url: string;
    teams: string[];
    priviliges: string[];
}

interface Tools {
    quantity: number;
    text: string;
    thumbnail: string;
}

export interface GuideDetails {
    guideid: string;
    image: Image;
    steps: Step[];
    comments: Comments;
    title: Title;
    author: Author[];
    introduction_raw: string;
    conclusion_raw: string;
    category: string;
    difficulty: string;
    tools: Tools[];
}

export interface GuideOverView {
    category: string;
    dataType: string;
    difficulty: string;
    guideid: number;
    image: Image;
    subject: string;
    type: string;
    title: string;
    url: string;
    media: Media[];
    revisionid: number;
}

